#ifndef _MEM_H_
#define _MEM_H_
#define _USE_MISC

#include <stddef.h>
#include  <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/mman.h>

#define  HEAP_START  ((void*)0x04040000)
#define BLOCK_MIN_SIZE 32
#define HEAP_PAGE_SIZE (4 * 1024)

struct mem;

#pragma pack(push, 1)
struct mem {
    struct mem * next;
    size_t capacity;
    bool is_free;
};
#pragma pack(pop)

void * mem_alloc(const size_t query);

void mem_free(void * mem);

void * heap_init(const size_t initial_size);

#define DEBUG_FIRST_BYTES 4

void mem_alloc_debug_struct_info(FILE * f, struct mem const * const address);

void mem_alloc_debug_heap(FILE * f, struct mem const * ptr);

#endif
