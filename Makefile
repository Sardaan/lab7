CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CC=gcc

all: main

main: main.c mem.c mem.h
	$(CC) -o main $(CFLAGS) main.c mem.c 

clean:
	rm -f main

